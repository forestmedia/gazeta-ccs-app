import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RecentService} from '../../providers/articles-service';
import { Box } from '../../components/box/box';

@Component({
  templateUrl: 'build/pages/recientes/recientes.html',
  providers:[RecentService],
   directives: [Box]

})
export class Recientes {
  public article:any = [];
  private start:Object ={};
  private busy:boolean = false;
  public mail:any = [];

  constructor(public navCtrl: NavController, public articleService:RecentService) {
    this.loadRecent();

  }

  loadRecent() {

    this.busy = true;

    return new Promise(resolve => {

      this.articleService.load(this.start)
      .then((data:any=[]) => {
        console.log(data.results);
        this.start=data.lastKey;
        for(let recent of data.results) {
          this.article.push(recent);
        }
        this.busy = false;
        resolve(true);
      });
    });
  }

  doInfinite(infiniteScroll:any) {
    if(!this.busy){
      this.loadRecent().then(()=>{
        infiniteScroll.complete();
      });
    }
  }


}
