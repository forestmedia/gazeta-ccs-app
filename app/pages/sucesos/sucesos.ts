import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RecentService} from '../../providers/articles-service';
import { SocialSharing } from 'ionic-native';
import {Article} from '../article/article';
import { Box } from '../../components/box/box';

@Component({
  templateUrl: 'build/pages/sucesos/sucesos.html',
  providers:[RecentService],
   directives: [Box]

})
export class Sucesos {
  public article:any = [];
  private start:Object ={};
  private busy:boolean = false;

  constructor(public navCtrl: NavController, public articleService:RecentService) {
    this.loadSection();
  }

  loadSection() {

    this.busy = true;

    return new Promise(resolve => {

      this.articleService.section('Sucesos',this.start)
      .then((data:any=[]) => {
        console.log(data.results);
        this.start=data.lastKey;
        for(let sections of data.results) {
          this.article.push(sections);
        }
        this.busy = false;
        resolve(true);
      });
    });
  }

  doInfinite(infiniteScroll:any) {
    if(!this.busy){
      this.loadSection().then(()=>{
        infiniteScroll.complete();
      });
    }
  }

  articleShare(slug, date){
    let url = 'http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/#!/articulo/'+slug+'/'+date;
    SocialSharing.share("Gazeta-Ccs: ",null/*Subject*/,null/*File*/,url)
    .then(()=>{
      //Success
      },
      ()=>{
         //Error
      })
  }

  doClickCard(item) {
    this.navCtrl.push(Article, {
      item: item
  });
  }
}
