import { Component, forwardRef} from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import {RecentService} from '../../providers/articles-service';
import { Box } from '../../components/box/box';

/*
  Generated class for the ArticlePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/tag/tag.html',
  providers:[RecentService],
  directives: [forwardRef(() => Box)],
})
export class Tag {
  public article:any = [];
  private start:Object ={};
  private busy:boolean = false;
  public mail:any = [];
  public hashtag:string;
  public stop:boolean = false;

  constructor(public navCtrl: NavController, private navParams: NavParams, public articleService:RecentService) {
    console.log('perra');
    let item = navParams.get('item');
    this.hashtag = item;
    this.loadTag();

  }

  loadTag() {

    this.busy = true;

    return new Promise(resolve => {

      this.articleService.section(this.hashtag,this.start)
      .then((data:any=[]) => {

        if(!data.lastKey){
          console.log('se acabo rodolfo');
          this.stop = true;
        }
        this.start=data.lastKey;
        for(let sections of data.results) {
          this.article.push(sections);
        }
        this.busy = false;
        resolve(true);
      });
    });
  }

  doInfinite(infiniteScroll:any) {
    if(!this.busy && !this.stop){
      console.log('una mierda');
      this.loadTag().then(()=>{
        infiniteScroll.complete();
      });
    }else if(this.stop){
      infiniteScroll.complete();
    }
  }

}
