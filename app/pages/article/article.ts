import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
/*
  Generated class for the ArticlePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/article/article.html',
})
export class Article {

  public article:any = [];
  constructor(private navCtrl: NavController, private navParams: NavParams) {
    let item = navParams.get('item');
    this.article.push(item);
  }

}
