import { Component, Input } from '@angular/core';
import {IONIC_DIRECTIVES} from 'ionic-angular';
import { SocialSharing } from 'ionic-native';
import {Article} from '../../pages/article/article';
import {Tag} from '../../pages/tag/tag';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'box',
  templateUrl: 'build/components/box/box.html',
  directives: [IONIC_DIRECTIVES]
})
export class Box {

public option:any;
@Input('article') article: any;

constructor(public navCtrl: NavController) {

  this.option = [
  { label: 'Política', value: 0 },
  { label: 'Economía', value: 1 },
  { label: 'Sucesos', value: 2 },
  { label: 'Opinión', value: 3 },
  { label: 'Imagen', value: 4}
  ];
}

articleShare(slug, date){
  let url = 'http://gazeta-ccs-files.s3-website-us-west-2.amazonaws.com/#!/articulo/'+slug+'/'+date;
  SocialSharing.share("Gazeta-Ccs: ",null/*Subject*/,null/*File*/,url)
  .then(()=>{
    //Success
    },
    ()=>{
       //Error
    })
}

doClickCard(item) {
  this.navCtrl.push(Article, {
    item: item
});
}

tagClick(tag) {
  this.navCtrl.push(Tag, {
    item: tag
});
}
}
