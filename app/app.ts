import { Component, ViewChild } from '@angular/core';
import { ionicBootstrap, Platform, Nav } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { Recientes } from './pages/recientes/recientes';
import { Politica } from './pages/politica/politica';
import { Economia } from './pages/economia/economia';
import { Sucesos } from './pages/sucesos/sucesos';
import { Opinion } from './pages/opinion/opinion';

@Component({
  templateUrl: 'build/app.html'
})
class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Recientes;

  pages: Array<{title: string, component: any}>;


  constructor(public platform: Platform) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Recientes', component: Recientes },
      { title: 'Política', component: Politica },
      { title: 'Economía', component: Economia },
      { title: 'Sucesos', component: Sucesos },
      { title: 'Opinión', component: Opinion }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

ionicBootstrap(MyApp);
