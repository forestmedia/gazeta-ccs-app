import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the RecentService provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RecentService {


  constructor(public http: Http) {

  }

  load(start:any = undefined) {

    return new Promise(resolve => {
      let obj:Object = {};
      console.log(start);
      if(start._id){
        obj = {lastKey:JSON.stringify(start)};
      }
      this.http.post('https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/articles', obj)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

  section(sec:string ,start:any = undefined) {

    return new Promise(resolve => {
      let obj:Object = {};

      console.log(start);
      if(start._id){
        obj = {lastKey:JSON.stringify(start)};
      }
      let url = 'https://swuv4lrpbk.execute-api.us-east-1.amazonaws.com/prod/tags/'+ sec + '?count=8';
      this.http.post(url, obj)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }
}
